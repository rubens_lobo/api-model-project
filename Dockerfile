#Contruir imagem
#    docker build -t <docker-name> -f ./Dockerfile .
#Rodar os testes
#    docker run -v "$PWD/target:/usr/target" <docker-name> mvn test -Denv=des
FROM maven:3.5.3-jdk-8-alpine
WORKDIR /usr
COPY . /usr
RUN mvn dependency:go-offline -B