package steps;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import model.PostModel;
import services.PostService;
import support.DataYaml;

import java.util.Map;

public class PostSteps {

    private PostService postService;
    private PostModel postModel;
    Map postData;

    public PostSteps() {
        this.postService = new PostService();

    }

    @Dado("tenha um post {string}")
    public void tenhaUmPost(String type) {
        this.postData = DataYaml.getMapYamlValues("Post",type);
        postService.setId((int)postData.get("id"));
    }

    @Quando("eu consultar o post")
    public void euConsultarOPost() {
        postService.consultar(postService.getId());
    }

    @Entao("deve retornar os dados do post {string}")
    public void deveRetornarOsDadosDoPost(String type) {
        postService.checkConsulta(type);
    }

    @Dado("eu tenho os dados para criar um post")
    public void euTenhoOsDadosParaCriarUmPost() {
        postModel = PostModel.builder()
                .title("Testing Title")
                .body("Testing Body")
                .userId(1)
                .build();
    }

    @Quando("eu criar o post")
    public void euCriarOPost() {
        postService.criar(postModel);
    }

    @Entao("deve ser registrado o post")
    public void deveSerRegistradoOPost() {
        postService.checkCriacao(postModel);
    }
}
