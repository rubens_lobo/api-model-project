package steps;

import cucumber.api.PendingException;
import cucumber.api.java.pt.*;
import org.junit.Assert;
import services.UsersService;
import io.restassured.response.Response;

import java.util.HashMap;

import static org.hamcrest.MatcherAssert.assertThat;

public class UsersSteps  {

    UsersService usersService;

    public UsersSteps() {
        usersService = new UsersService();
    }

    @Dado("que eu tenha o nome do meu cliente {string}")
    public void queEuTenhaONomeDoMeuCliente(String titulo) {
        usersService.getNome(titulo);
    }
    @Quando("eu consultar ele pelo nome")
    public void eu_consultar_ele_pelo_nome() {
        usersService.get(usersService.getName());
    }
    @Entao("devo receber os dados do seu cadastro com {string}")
    public void devoReceberOsDadosDoSeuCadastroCom(String tipo) {
        usersService.checkConsulta(tipo);
    }
}
