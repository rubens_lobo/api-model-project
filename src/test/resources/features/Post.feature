# language: pt
# charset: UTF-8
Funcionalidade: Post
  EU como cliente
  GOSTARIA consultar meus clientes
  PARA saber quem eu estou atendendo

  # Supported severity values: blocker, critical, normal, minor, trivial. ex.: @severity=critical
  # Every Feature or Scenario can be annotated by following tags: @flaky, @muted, @known

   @severity=critical
  Esquema do Cenario: Consultar Post <TYPE>
    Dado tenha um post "<TYPE>"
    Quando eu consultar o post
    Entao deve retornar os dados do post "<TYPE>"

    Exemplos:
      | TYPE     |
      | valido   |
      | invalido |

  @dev
  Cenario: Criar Post
    Dado eu tenho os dados para criar um post
    Quando eu criar o post
    Entao deve ser registrado o post
