# language: pt
# charset: UTF-8

@PM-2
Funcionalidade: Users
  EU como cliente
  GOSTARIA consultar meus clientes
  PARA saber quem eu estou atendendo

  # Supported severity values: blocker, critical, normal, minor, trivial. ex.: @severity=critical
  # Every Feature or Scenario can be annotated by following tags: @flaky, @muted, @known

  @severity=critical @known
  Cenário: Consultar um cliente existente
    Dado que eu tenha o nome do meu cliente "usuario existente"
    Quando eu consultar ele pelo nome
    Entao devo receber os dados do seu cadastro com "success"

  @severity=minor
  Cenário: Consultar um cliente inexistente
    Dado que eu tenha o nome do meu cliente "usuario inexistente"
    Quando eu consultar ele pelo nome
    Entao devo receber os dados do seu cadastro com "not found"