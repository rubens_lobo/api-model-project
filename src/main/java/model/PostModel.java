package model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter @Builder
public class PostModel {

    public String title;
    public String body;
    public Integer userId;
}
