package services;

import support.DataYaml;
import support.Spec;
import io.restassured.response.Response;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@Log4j2
@Getter
@Setter
public class UsersService {

    private String name;
    private Response response;

    public void getNome(String titulo){
        log.info("Pega o nome do cliente");
        setName(DataYaml
                .getMapYamlValues("Usuarios",titulo)
                .get("name"));
    }

    public void get(String name){
        log.info("Consulta o nome do cliente");
        String URI = "/users?name=".concat(name);
        setResponse(given()
                .spec(Spec.getSpec())
                .when()
                .get(URI));
    }

    public void checkConsulta(String tipo){
        log.info("Valida se os dados do cliente foi apresentado");
        switch(tipo) {
            case "success":
                response.then()
                        .statusCode(200)
                        .body("id[0]",equalTo(1))
                        .body("name[0]", containsString(getName()))
                        .body("address.street[0]", notNullValue());

                assertThat(response.getBody().asString(), matchesJsonSchemaInClasspath("schemas/users/users-schema.json"));
                break;
            case "not found":
                response.then()
                        .statusCode(200)
                        .body(notNullValue());
                break;
            default:
                throw new RuntimeException("Tipo não encontrado!");
        }
    }
}
