package services;

import io.restassured.response.Response;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import model.PostModel;
import org.hamcrest.Matchers;
import support.DataYaml;
import support.Spec;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.lessThan;

@Log4j2
@Getter
@Setter
public class PostService {

    private int id;
    private Response response;

    public void consultar(int id){
        response = given().spec(Spec.getSpec())
                .when()
                .get("/posts/"+id);
    }

    public void checkConsulta(String type){

        switch(type) {
            case "valido":
                response.then()
                        .statusCode(200)
                        .body("userId", equalTo(1))
                        .body("title",notNullValue())
                        .time(lessThan(4000L));

                assertThat(response.getBody().asString(),
                        matchesJsonSchemaInClasspath("schemas/post/post-schema.json"));
                break;
            case "invalido":
                response.then()
                        .statusCode(404)
                        .body("isEmpty()", Matchers.is(true));
                break;
            default:
                throw new RuntimeException("Type invalido!");
        }
    }

    public void criar(PostModel postModel){
        response = given().spec(Spec.getSpec())
                .when()
                .body(postModel)
                .post("/posts");
    }

    public void checkCriacao(PostModel postModel){
        response.then()
                .statusCode(201)
                .body("title", equalTo(postModel.getTitle()))
                .body("body",equalTo(postModel.getBody()))
                .body("userId",equalTo(postModel.getUserId()))
                .body("id",notNullValue());

        assertThat(response.getBody().asString(),
                matchesJsonSchemaInClasspath("schemas/post/post-schema.json"));
    }


}
