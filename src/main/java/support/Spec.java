package support;

import com.github.javafaker.Faker;

import support.config.Configuration;
import io.qameta.allure.Allure;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.aeonbits.owner.ConfigCache;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.io.output.WriterOutputStream;
import java.io.PrintStream;
import java.io.StringWriter;
import java.util.Locale;

@Log4j2
public class Spec {
    @Getter @Setter
    public static RequestSpecification spec;
    @Getter @Setter
    public static StringWriter requestWriter;
    @Getter @Setter
    public static PrintStream requestCapture;
    @Getter @Setter
    public static StringWriter responseWriter;
    @Getter @Setter
    public static PrintStream responseCapture;
    @Getter @Setter
    public static Configuration configuration;
    @Getter @Setter
    private static Faker faker;

    public void setMonitoramento() {
        log.info("Preparando objetos para monitorar a requisição e a resposta do servidor");
        requestWriter = new StringWriter();
        requestCapture = new PrintStream(new WriterOutputStream(requestWriter), true);
        responseWriter = new StringWriter();
        responseCapture = new PrintStream(new WriterOutputStream(responseWriter), true);
    }

    public void setLanguageSyntethicData(){
        faker = new Faker(new Locale(configuration.fakerLanguage()));
    }

    public void setConfigProperties() {
        log.info("Preparando objetos para pegar as configuracoes do arquivos de properties");
        ConfigFactory.setProperty("env", System.getProperty("env"));
        setConfiguration(ConfigCache.getOrCreate(Configuration.class));
    }

    public void setSpecStandard() {
        log.info("Preparando objetos base com as informacoes base para as requisições");
        spec = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri(configuration.urlBase())
                .addFilter(new RequestLoggingFilter(requestCapture))
                .addFilter(new ResponseLoggingFilter(responseCapture))
                .build();
    }

    public void setReportStandard(){
        log.info("Gerando informações no relatório de Teste sobre a requisição e respostas");
        Allure.addAttachment(AllureTypeFile.REQUEST, requestWriter.toString());
        Allure.addAttachment(AllureTypeFile.RESPONSE, responseWriter.toString());
    }
}
